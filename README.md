ElasticSearch - Logstash - Kibana playbook
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This playbook will install an ELK 5.x stack, including 'filebeat' on a single node Ubuntu system.

Prerequisites on the target system:
    sudo apt-get install python python-passlib

Deploy:
    virtualenv venv
    source venv/bin/activate
    pip install --no-deps -r requirements.txt
    ansible-playbook --become --inventory '<hostname>,' elk.yml


When the deployment finished successfully, you should be able to connect to
the Kibana interface on http://<hostname>:8088/ with credentials 'admin:admin'


Customize:
   - Default ports are defined in group_vars/all.yml and can be changed if required.
   - All application templates and variables can be customized.
